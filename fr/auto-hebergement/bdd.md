Les bases de données
====================

Une base de données permet à une application de retrouver rapidement des
informations. Cela est particulièrement important lorsqu’il y a beaucoup
de données reliées entre elles.

Par exemple, si on prend le cas d’un blog, alors les commentaires
peuvent être stockés dans une base de données. Chaque commentaire est
fait sur un certain article, par un visiteur donné, à une date précise.
Le commentaire comme l’article ont un lien bien précis. L’utilisateur
peut avoir donné son adresse e-mail pour être averti de nouveaux
messages…

Vous l’aurez compris, toutes ces données s’entrecroisent, et il est plus
efficace d’utiliser une base de données pour les retrouver rapidement.

Cependant, ce n’est pas forcément obligatoire. Surtout sur un serveur
auto-hébergé, où vous n’aurez sans doute pas des milliers
d’utilisateurs.

Comprenez donc bien que si vous pouvez choisir des applications qui
n’ont pas besoin de base de données, c’est un avantage pour vous car ça
fait ça de moins à administrer, et ça de moins à sécuriser. Eh oui, car
une base de données peut elle aussi subir des attaques.

Une alternative est d’utiliser dans ce cas SQLite, puisque cette base de
données ne nécessite pas d’administration particulière, c’est
l’application qui se chargera de tout.

SQlite
------

[SQLite](https://www.sqlite.org/) est un moteur de base de données tout
simplement génial.

![](images/sqlite.png)

Vous n’avez rien de particulier à faire pour l’administrer, c’est
l’application qui en a besoin qui se chargera de créer la base. En plus,
c’est très facile à sauvegarder puisque c’est dans ce cas un simple
fichier. Enfin, ce moteur sait se montrer léger et fonctionne bien même
sur du matériel moins puissant.

Alors certains diront que ce n’est pas le moteur le plus performant.
C’est vrai. Mais à moins d’avoir des milliers de visiteurs sur votre
site, vous ne verrez pas la différence et eux non plus. N’hésitez pas,
il y a plus d’avantages que d’inconvénients à utiliser SQLite en
auto-hébergement.

MySQL
-----

MySQL est un autre moteur de base données, sans doute le plus répandu.
Il est distribué sous une licence libre et sous une licence propriétaire
(vous êtes prévenu).

Puisqu’une fois installé, MySQL sera lancé en arrière-plan, il faudra
vous renseigner sur la sécurisation de ce service, en particulier
ajouter à fail2ban la surveillance de MySQL (voir [Fail2ban](securite.html#fail2ban))

### Installation de MySQL

Pour installer MySQL, il faut installer le paquet `mysql-server-5.5`. Si
c’est une application en php qui a besoin de mysql, alors il faudra
aussi le paquet `php5-mysql`.

Lors de son installation, il vous sera demandé un mot de passe
administrateur. Ce mot de passe est à bien retenir, puisqu’il vous sera
nécessaire ensuite pour créer de nouvelles bases.

### Gérer MySQL

Pour la suite, vous pourrez administrer mysql après avoir entré la
commande suivante :

```
mysql -u root -p
```

Une fois vos modifications effectuées, vous pourrez quitter avec la
combinaison de touches ctrl+d.

#### Créer une nouvelle base de données

```
mysql> CREATE DATABASE nom_de_la_nouvelle_base;
```

#### Créer un utilisateur de mysql

```
mysql> CREATE USER 'utilisateur'@'localhost' IDENTIFIED BY 'motdepasse';
```

Remplacez bien sûr `utilisateur` et `motdepasse`.

#### Modifier les droits

```
mysql> GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, ALTER,
    CREATE TEMPORARY TABLES, LOCK TABLES ON nom_de_la_base.* TO
    'utilisateur'@'localhost';
```

Quelques explications :

-   `GRANT` : donne les droits suivants
-   `SELECT, INSERT`… : différentes actions sur la base de données
-   `ON nom_de_la_base` : ces droits sont donnés sur la base qui
    porte le nom `nom_de_la_base`
-   `TO ’utilisateur’@’localhost’` : les droits sont donnés à
    l’utilisateur sur la machine locale

PostgreSQL
----------

PostgreSQL est un autre moteur de base de données, entièrement libre.

### Installation de PostgreSQL

Pour installer PostgreSQL, il faut installer les paquets suivants :

```
# apt-get install postgresql postgresql-client postgresql-client-common
```

Et pour qu’une application php puisse accéder au moteur postgreSQL,
installez le paquet `php5-pgsql`.

### Gérer PostgreSQL

Pour se connecter à postgreysql, on utilise la commande `su postgres -c
psql`.

Voici quelques commandes permettant de gérer PostgreSQL.

#### Modifier le mot de passe administrateur

```
su postgres -c psql ALTER USER postgres WITH PASSWORD 'mot_de_passe';
```

#### Ajouter un utilisateur à la base

```
su postgres -c psql CREATE USER 'nouvelutilisateur'
WITH PASSWORD 'mot_de_passe_de_l_utilisateur';
```

#### Créer une nouvelle base

```
su postgres -c psql << EOF
\connect template1
CREATE DATABASE "nom_de_base" WITH ENCODING 'UTF-8';
GRANT ALL PRIVILEGES ON DATABASE "nom_de_base" TO "utilisateur";
ALTER DATABASE "nom_de_base" OWNER TO "utilisateur";
\q
```
