# Installation de Framaslides

<p class="alert alert-warning">
  <span class="label label-primary">Attention</span> La peinture est
  fraîche, cette page n'est pas terminée et est amenée à évoluer.
</p>

[Framaslides](https://framaslides.org) est une application développée par Framasoft qui
permet de créer et gérer ses présentations en ligne. Elle est basée sur
le logiciel libre [Strut](http://strut.io) disponible sous licence AGPL-3.0.

<div class="alert alert-info">
  Ce guide est prévu pour Debian Jessie, mais fonctionera également sur
  Debian Stretch.
</div>

## 1 - Nourrir la terre

![](images/icons/preparer.png)

### Installation

Toutes les commandes ci-dessous devront être utilisées en tant que `root`.

Commençons par vérifier que notre serveur est à jour. Pensez à vérifier
les programmes impactés si votre machine sert à plusieurs usages :

    apt update && apt dist-upgrade

Nous avons tout d'abord besoin d'un serveur web. Nous utiliserons ici
le serveur Nginx.

    apt install nginx

Ensuite, nous aurons besoin de NodeJS dans une version récente pour les
bibliothèques Javascript. Nous ajoutons donc les dépôts NodeSource,
en commençant par ajouter leur clé.

    curl -s https://deb.nodesource.com/gpgkey/nodesource.gpg.key | apt-key add -

Ensuite, nous ajoutons le dépôt lui-même :

    echo 'deb https://deb.nodesource.com/node_6.x jessie main' > /etc/apt/sources.list.d/nodesource.list

On fait de même pour obtenir PHP 7 :

    curl -s https://www.dotdeb.org/dotdeb.gpg | apt-key add -
    echo 'deb http://packages.dotdeb.org jessie all' > /etc/apt/sources.list.d/dotdeb.list

On rafraichit les dépôts, puis on installe nodejs et php et quelques-unes
de ses extensions, ainsi que le système de base de données PostgreSQL
et quelques utilitaires :

    apt update && apt install nodejs php-fpm php-cli php-xml php-gd php-mbstring php-intl php-pgsql php-json php-tidy php-curl php7.0-apc postgresql unzip git

## 2 - Semer

![](images/icons/semer.png)

On récupère la dernière version de Framaslides (actuellement la 1.0.2)
sur la page https://framagit.org/framasoft/framaslides/tags et on
télécharge les sources au format .zip.

    curl -o framaslides.zip https://framagit.org/framasoft/framaslides/repository/archive.zip?ref=1.0.2

On dézippe les sources.

    unzip framaslides.zip

On renomme le dossier récupéré en quelque chose de plus utilisable

    mv framaslides-* framaslides

On bouge ce dossier dans le répertoire pour les applications web sous Debian :

    mv framaslides /var/www/html/

On donne les bons droits au dossier

    chown -R www-data:www-data /var/www/html/framaslides/

On crée un nouveau fichier de configuration nginx pour framaslides :

    nano /etc/nginx/sites-available/slides.conf

Où l'on colle le squelette suivant :

    server {
        listen *:80;
        listen [::]:80;

        # listen *:443 ssl;
        # ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
        # listen [::]:443 ssl;
        # ssl_certificate /path/to/certificate;
        # ssl_certificate_key /path/to/associated/key;

        server_name slides.mondomaine.tld ;

        root   /var/www/html/framaslides;

        # if ($scheme != "https") {
        #    rewrite ^ https://$http_host$request_uri? permanent;
        # }

        index index.html index.htm index.php index.cgi index.pl index.xhtml;

        location = /favicon.ico {
            log_not_found off;
            access_log off;
        }

        location = /robots.txt {
            allow all;
            log_not_found off;
            access_log off;
        }

        location ~ \.php$ {
            location ~ ^/app\.php(/|$) {
                root   /var/www/html/framaslides/web/;
                fastcgi_pass unix:/run/php/php7.0-fpm.sock;
                fastcgi_split_path_info ^(.+\.php)(/.*)$;
                include fastcgi_params;
                fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
                fastcgi_param DOCUMENT_ROOT $realpath_root;
                internal;
            }
            return 404;
        }

        location @php {
            try_files $uri =404;
            include /etc/nginx/fastcgi_params;
            fastcgi_pass unix:/run/php/php7.0-fpm.sock;
            fastcgi_index index.php;
            fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
            fastcgi_intercept_errors on;
        }


        location / {
            root   /var/www/html/framaslides/web/;
            try_files $uri /app.php$is_args$args;
        }
    }

<div class="alert alert-info">
  Les zones commentées sont à enlever si vous utilisez https
  (par exemple avec Let's Encrypt).
</div>

On active le fichier de configuration :

    ln -s /etc/nginx/sites-available/slides.conf /etc/nginx/sites-enabled/slides.conf

On teste si le fichier de configuration est valide.

    nginx -t

<div class="alert alert-warning">
  Vous voudrez peut-être désactiver le fichier de configuration par
  défaut : <pre>rm /etc/nginx/sites-enabled/default</pre>
</div>

Si c'est bon, on peut recharger le serveur nginx avec la nouvelle configuration

    systemctl reload nginx

## Arroser

![](images/icons/arroser.png)


Nous aurons besoin d'une base de données postgresql, connectons-nous donc à postgresql

    sudo -i -u posgres
    psql

Nous allons créer une base de données slides et un utilisateur associé

    CREATE DATABASE slides;
    CREATE USER slides WITH PASSWORD 'un mot de passe compliqué';
    GRANT ALL PRIVILEGES ON DATABASE slides TO slides;

Quittons maintenant postgresql `\q`

Installons les outils grunt et Composer :

    npm i -g grunt-cli webpack

Pour Composer, c'est plus compliqué :

    php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
    php -r "if (hash_file('SHA384', 'composer-setup.php') === '669656bab3166a7aff8a7506b8cb2d1c292f042046c5a994c43155c0be6190fa0355160742ab2e1c88d40d5be660b410') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
    php composer-setup.php
    php -r "unlink('composer-setup.php');"
    mv composer.phar /usr/local/bin/composer

Pour installer les bibiothèques Javascript, npm a besoin d'un répertoire `.npm` dans `/var/www/`.

    mkdir /var/www/.npm
    chown -R www-data:www-data /var/www/.npm

Pareil pour Composer et les bibliothèques PHP

    mkdir /var/www/.composer
    chown -R www-data:www-data /var/www/.composer

Connectons-nous ensuite en tant que l'utilisateur `www-data`

    su www-data -s /bin/bash

Et commençons par installer les dépendances Javascript.

    npm i

Cela peut prendre un moment selon les performances de votre machine.

À présent, construisons les dépendances Javascript

    grunt build

Puis d'autres

    webpack

À présent, nous pouvons installer les dépendances PHP. Nous aurons besoin
de l'outil \[Composer\]\[3\], que nous allons utiliser ainsi :

    SYMFONY_ENV=prod composer install --no-dev -o --prefer-dist

Une fois que tout sera installé, Composer va vous demander les
paramètres suivants :

*   `database_host` L'hôte de votre serveur de base de données.
    Laissez la valeur par défaut.
*   `database_port` Le port du serveur de base de données. Entrez 5432
*   `database_name` Le nom de la base de données. Entrez *slides*
*   `database_user` L'utilisateur de la base de données. Entrez *slides*
*   `database_password` Le nom de la base de données.
    Entrez le mot de passe défini plus haut.

Les valeurs de `mailer_*` peuvent être laissées par défaut, ce tutoriel
ne couvrant pas l'installation d'un serveur de mail.

Le `secret` est à remplir avec une chaine de caractères alétoire assez longue.

On peut ensuite mettre à jour la base de données

    bin/console doctrine:schema:update --env=prod --force

## Regarder pousser

![](images/icons/pailler.png)

Créons un administrateur pour le serveur. Remplacez `utilisateuradmin`
par le nom de votre choix. Il vous sera demandé une adresse email et un
mot de passe.

    bin/console fos:user:create utilisateuradmin --env=prod --super-admin
