# Installation de uMap

![](images/umap/umap.jpg)

[uMap][1] est le logiciel de personnalisation de cartes géographiques
que nous proposons sur [Framacarte][2] en partenariat avec OpenStreetMap France.

![](images/umap/framacarte.png)

Voici un tutoriel pour vous aider à l’installer sur votre serveur.

<p role="alert" class="alert alert-info">
  <span class="label label-primary">Informations</span><br /> Dans la
  suite de ce tutoriel, les instructions seront données pour un serveur
  dédié sous Debian Jessie avec une base de données PostgreSQL et un
  serveur web NginX.<br />
  Nous supposerons que vous avez déjà fait pointer votre nom de domaine
  sur votre serveur auprès de votre
  <a href="http://fr.wikipedia.org/wiki/Bureau_d%27enregistrement">registraire</a>.
</p>

## Prérequis

### Python 3.4, PostgreSQL, PostGIS, Git

Pour faire fonctionner uMap, il est nécessaire d’installer Python 3.4,
PostgreSQL 9.4, PostGIS et quelques dépendances. Sur Debian :

    apt-get install postgresql postgresql-server-dev-9.4 postgis python-pip git python-virtualenv virtualenvwrapper python3-dev python3-setuptools libtiff5-dev libjpeg62-turbo-dev zlib1g-dev libfreetype6-dev liblcms2-dev libwebp-dev tcl8.6-dev tk8.6-dev python-tk python3.4-dev python-dev
    apt-get build-dep python-imaging

## Installation

### 1 - Préparer la terre

![](images/icons/preparer.png)

Tout d'abord, connectez-vous en tant que `root` sur votre serveur et
créez un compte utilisateur `umap` ainsi que son dossier `/home/umap`
dans lequel seront copiés les fichiers avec les droits d'accès correspondants.

    useradd -N umap -m
    usermod -s /bin/bash umap

### 2 - Semer

![](images/icons/semer.png)

Connectez-vous avec l'utilisateur `umap` : `su umap`
Téléchargez le dépôt git officiel dans un dossier `/home/umap/src`

    cd /home/umap
    git clone https://github.com/umap-project/umap src

Créez un environnement virtuel Python :

    mkvirtualenv umap --python=`which python3.4`
    source .virtualenvs/umap/bin/activate

Installez les librairies requises :

    cd src
    pip install django-appconf
    pip install -r requirements.txt

### 3 - Arroser

![](images/icons/arroser.png)

#### PostgreSQL

Il faut maintenant créer la base de données et configurer uMap.
Ajoutez tout d’abord un utilisateur `umap` et sa base de
données associée dans PostgreSQL :

    sudo -u postgres createuser umap
    sudo -u postgres createdb umap -O umap

Puis crée l'extension PostGis

    sudo -u postgres psql umap
    CREATE EXTENSION postgis;

(`\q` pour sortir de `psql`)

#### uMap

Maintenant que la base de données est prête, il faut configurer uMap.
Éditez le fichier `umap/settings/local.py` sur le modèle du fichier `local.py.sample` :

    cp umap/settings/local.py.sample umap/settings/local.py
    nano umap/settings/local.py</pre>

Dans ce fichier, il est important de définir les paramètres :

*   `SECRET_KEY` : une longue chaîne de caractères aléatoires
*   `DEBUG = False`
*   `ADMINS` avec vos identifiants et adresse email
*   `SOCIAL_AUTH_OPENSTREETMAP_KEY`, `SOCIAL_AUTH_OPENSTREETMAP_SECRET`
    et `AUTHENTICATION_BACKENDS` si vous voulez proposer à vos
    utilisateurs d’avoir un compte pour gérer leurs cartes
    (nous recommandons OpenStreetMap ici mais vous pouvez aussi utiliser Twitter, Github ou Bitbucket)
*   `UMAP_DEMO_SITE = False`
*   `SITE_URL = "https://votre-site.org"`
*   `SHORT_SITE_URL = "https://votre-site.org"` (vous pouvez utiliser un
    nom de domaine plus court s’il est configuré comme un alias du nom de domaine principal)
*   `MAPQUEST_KEY` pour permettre la recherche de lieu

##### Clé OpenStreetMap et MapQuest

Pour obtenir les clés OpenStreetMap, il faut que vous ayez enregistré un
compte sur [openstreetmap.org][3].
Dans « Mes options », « paramètres OAuth » ajoutez une « application cliente ».

![](images/umap/osm-key-1.png)

![](images/umap/osm-key-2.png)

Pour obtenir la clé MapQuest, même chose : rendez-vous sur
[developer.mapquest.com][4] et remplissez le formulaire pour obtenir
votre clé d’<abbr>API</abbr>.

![](images/umap/mapquest-key-2.png)

Le principe est quasiment le même pour Twitter, Github et Bitbucket.

Il reste maintenant à remplir la base de données, importer en cache les
fichiers statiques, créer le compte administrateur d’uMap.

    python manage.py migrate
    python manage.py collectstatic
    python manage.py createsuperuser

### 4 - Pailler

![](images/icons/pailler.png)

À ce stade, si tout s'est bien passé, lorsque vous exécutez cette commande :

    python manage.py runserver 0.0.0.0:8019

uMap est fonctionnel.
La page d’accueil sur l'URL `http://ip_de_votre_serveur:8019/` tourne
mais il ne sera pas encore possible de créer des cartes tant qu’on aura
pas ajouté des fonds de carte.

#### Fonds de carte

Pour cela, connectez-vous avez le compte admin précédemment créé sur
`http://ip_de_votre_serveur:8019/admin` et cliquez sur « Tile layers → Ajouter ».

![](images/umap/umap-tile-layers-1.png)

![](images/umap/umap-tile-layers-2.png)

Voici ci-dessous la liste des fonds de carte présents sur Framacarte :

<table class="table table-bordered">
  <caption class="sr-only">Fonds de carte de Framacarte</caption><thead>
    <tr>
      <th scope="col">
        Nom
      </th>

      <th scope="col">
        <abbr>URL</abbr>
      </th>

      <th scope="col">
        Attribution
      </th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>
        Positron
      </td>
      <td>
        https://cartodb-basemaps-{s}.global.ssl.fastly.net/light_all/{z}/{x}/{y}.png
      </td>
      <td>
        Map tiles by <a href="http://cartodb.com/attributions#basemaps">CartoDB</a>, under <a href="https://creativecommons.org/licenses/by/3.0/">CC BY 3.0</a>. Data by <a href="http://www.openstreetmap.org/">OpenStreetMap</a>, under ODbL.
      </td>
    </tr>
    <tr>
      <td>
        Dark Matter
      </td>
      <td>
        https://cartodb-basemaps-{s}.global.ssl.fastly.net/dark_all/{z}/{x}/{y}.png
      </td>
      <td>
        Map tiles by <a href="http://cartodb.com/attributions#basemaps">CartoDB</a>, under <a href="https://creativecommons.org/licenses/by/3.0/">CC BY 3.0</a>. Data by <a href="http://www.openstreetmap.org/">OpenStreetMap</a>, under ODbL.
      </td>
    </tr>
    <tr>
      <td>
        OpenCycleMap
      </td>
      <td>
        https://{s}.tile.thunderforest.com/cycle/{z}/{x}/{y}.png
      </td>
      <td>
        “OpenCycleMap” and “OpenStreetMap contributors”
      </td>
    </tr>
    <tr>
      <td>
        Outdoors
      </td>
      <td>
        https://{s}.tile.thunderforest.com/outdoors/{z}/{x}/{y}.png
      </td>
      <td>
        Tiles © <a href="http://www.thunderforest.com/outdoors/">Gravitystorm</a> / map data <a href="http://osm.org/copyright">OpenStreetMap</a>
      </td>
    </tr>
  </tbody>
</table>

#### Processus silencieux - UWSGI

Pour lancer uMap en tant que service afin d'éviter devoir garder un
terminal ouvert en permanence, on utilise uwsgi.
Installez uwsgi `apt-get install uwsgi`.
Dans le dossier `/home/umap` créer un fichier `uwsgi.ini` contenant :

    [uwsgi]
    uid = umap
    gid = users
    # Django-related settings
    # the base directory (full path)
    chdir           = /home/umap/src
    # Django's wsgi file
    module          = umap.wsgi
    # the virtualenv (full path)
    home            = /home/umap/.virtualenvs/umap

    # process-related settings
    # master
    master          = true
    # maximum number of worker processes
    processes       = 8
    # the socket (use the full path to be safe
    socket          = /home/umap/umap.sock
    # ... with appropriate permissions - may be needed
    chmod-socket    = 666
    # clear environment on exit
    vacuum          = true
    plugins         = python3

Ainsi qu'un fichier `uwsgi_params` (utilisé par Nginx) :

    uwsgi_param  QUERY_STRING       $query_string;
    uwsgi_param  REQUEST_METHOD     $request_method;
    uwsgi_param  CONTENT_TYPE       $content_type;
    uwsgi_param  CONTENT_LENGTH     $content_length;

    uwsgi_param  REQUEST_URI        $request_uri;
    uwsgi_param  PATH_INFO          $document_uri;
    uwsgi_param  DOCUMENT_ROOT      $document_root;
    uwsgi_param  SERVER_PROTOCOL    $server_protocol;
    uwsgi_param  REQUEST_SCHEME     $scheme;
    uwsgi_param  HTTPS              $https if_not_empty;

    uwsgi_param  REMOTE_ADDR        $remote_addr;
    uwsgi_param  REMOTE_PORT        $remote_port;
    uwsgi_param  SERVER_PORT        $server_port;
    uwsgi_param  SERVER_NAME        $server_name;

et créer un lien symbolique pour activer « l’application » dans <abbr>UWSGI</abbr>

    sudo ln -s /home/umap/uwsgi.ini /etc/uwsgi/apps-enabled/umap.ini
    service uwsgi restart

Pour lancer uMap, il suffira maintenant de taper `service uwsgi start umap`

#### Nginx

Nous allons maintenant configurer uMap pour le rendre accessible depuis
un nom de domaine avec Nginx.
Installez nginx `apt-get install nginx`.
Dans le dossier `/etc/sites-available`, en tant que `root`, créez un
fichier `votre-site.org.vhost` s'il n'existe pas déjà contenant :

    # the upstream component nginx needs to connect to
    upstream umap {
        server unix:///home/umap/umap.sock;
    }

    proxy_cache_path /tmp/nginx_ajax_proxy_cache levels=1:2 keys_zone=ajax_proxy:10m inactive=60m;
    proxy_cache_key "$args";
    # configuration of the server
    server {
        # the port your site will be served on
        listen      80;
        listen   [::]:80;
        listen      443 ssl;
        listen   [::]:443 ssl;
        # the domain name it will serve for
        server_name votre-site.org www.votre-site.org;
        charset     utf-8;

        # https redirection
        if ($scheme = http) {
            return 301 https://votre-site.org$request_uri;
        }
        if ($host = www.votre-site.org) {
            return 301 https://votre-site.org$request_uri;
        }

        # SSL conf
        ssl_certificate      /etc/ssl/private/votre-site.org.chained.pem;
        ssl_certificate_key  /etc/ssl/private/votre-site.org.key;
        ssl_session_timeout 5m;
        ssl_session_cache shared:SSL:5m;
        ssl_dhparam /etc/ssl/private/dh2048.pem;
        ssl_prefer_server_ciphers on;
        ssl_protocols TLSv1 TLSv1.1 TLSv1.2; # not possible to do exclusive
        ssl_ciphers 'EDH+CAMELLIA:EDH+aRSA:EECDH+aRSA+AESGCM:EECDH+aRSA+SHA384:EECDH+aRSA+SHA256:EECDH:+CAMELLIA256:+AES256:+CAMELLIA128:+AES128:+SSLv3:!aNULL:!eNULL:!LOW:!3DES:!MD5:!EXP:!PSK:!DSS:!RC4:!SEED:!ECDSA:CAMELLIA256-SHA:AES256-SHA:CAMELLIA128-SHA:AES128-SHA';

        # OCSP conf
        ssl_stapling on;
        ssl_stapling_verify on;
        resolver_timeout 5s;
        resolver 80.67.188.188 [2001:913::8] 80.67.169.12 valid=300s;

        # STS conf
        add_header Strict-Transport-Security max-age=15768000; # six months

        # max upload size
        client_max_body_size 25M;   # adjust to taste

        # Django media
        location /uploads/  {
            alias /home/umap/.virtualenvs/umap/var/uploads/;
            expires 30d;
        }

        location /static/ {
            alias /home/umap/.virtualenvs/umap/var/static/;
        }

        # X-Accel-Redirect
        location /internal/ {
            gzip off;
            add_header Content-Encoding gzip;
            internal;
            alias /home/umap/.virtualenvs/umap/var/uploads/;
        }

        location /ajax-proxy/ {
            valid_referers server_names;
            if ($invalid_referer) {
                return 404;
            }
            if ($args !~ ^url=(.*)$) {
                return 404;
            }
            if ($args ~ ^url=(.*)$) {
                set $target $1;
            }
            add_header X-Proxy-Cache $upstream_cache_status;
            proxy_pass $target;
            proxy_cache ajax_proxy;
            proxy_cache_valid 3m;
        }

        # Finally, send all non-media requests to the Django server.
        location / {
            uwsgi_pass  umap;
            include     /home/umap/uwsgi_params;
        }
    }

Copiez votre clé et votre certificat <abbr>SSL</abbr> (`votre-site.org.key`
et `votre-site.org.chained.pem`) pour l'utilisation du <abbr>https<abbr>
dans le dossier `/etc/ssl/private/`.

Créez un lien symbolique pour activer le site et enfin, relancez Nginx :

    sudo ln -s /etc/nginx/sites-available/votre-site.org /etc/nginx/sites-enabled/votre-site.org
    service nginx restart

<i class="glyphicon glyphicon-tree-deciduous"></i>

 [1]: https://github.com/umap-project/umap/
 [2]: https://framacarte.org
 [3]: https://openstreetmap.org
 [4]: https://developer.mapquest.com/plan_purchase/steps/business_edition/business_edition_free/register