# Installation de Framadate

![](images/framadate/logo-framadate.png)

[Framadate][1] est le logiciel de création de sondages que nous
développons et proposons comme service en ligne.
Il est initialement basé sur le logiciel Studs développé par
l’université de Strasbourg et que nous avons largement remanié.

![](images/framadate/framadate.png)

Voici un tutoriel pour vous aider à l’installer sur votre serveur.

<p class="alert alert-info">
  <span class="label label-primary">Informations</span><br /> Dans la
  suite de ce tutoriel, les instructions seront données pour un serveur
  dédié sous Debian avec une base de données MySQL, un serveur web Apache
  (ou NginX), PHP 5.4 (minimum) et Postfix (ou Exim ; pour l’envoi des courriels)
   d'installés.<br />
   Nous supposerons que vous avez déjà fait pointer votre nom de domaine
   sur votre serveur auprès de votre
   <a href="http://fr.wikipedia.org/wiki/Bureau_d%27enregistrement">registraire</a>.
</p>

## Installation

### 1 - Préparer la terre

![](images/icons/preparer.png)

Tout d’abord, connectez-vous en tant que `root` sur votre serveur et
créez un compte utilisateur `framadate` ainsi que le dossier `/var/www/framadate`
dans lequel seront copiés les fichiers avec les droits d’accès correspondants.

    useradd framadate
    groupadd framadate
    mkdir /var/www/framadate
    chown framadate:framadate -R /var/www/framadate

### 2 - Semer

![](images/icons/semer.png)

Connectez-vous avec l’utilisateur `framadate` : `su framadate -s /bin/bash`

Puis, téléchargez la dernière [version stable de Framadate](https://framasoft.frama.io/framadate/latest.zip) qui inclus les librairies nécessaires.
Et dézippez son contenu sur le serveur.


### 3 - Arroser

![](images/icons/arroser.png)

#### MySQL

Il faut maintenant créer la base de données et configurer Framadate.

Installez tout d’abord le paquet `mysql-server` (notez le mot de passe
root) et démarrez MySQL : `service mysql start`

Créez un utilisateur et une base de données `framadate`.
Sur votre serveur dédié vous pouvez utiliser [PhpMyAdmin][4] qui est
souvent pré-installé, sinon voici comment faire avec [Adminer][5].

Téléchargez Adminer en ligne de commande (toujours depuis le dossier `/var/www/framadate`).

    wget -O adminer.php http://www.adminer.org/latest-mysql.php

Connectez-vous avec le compte root MySQL sur `votre-nom-de-domaine.org/adminer.php`
et cliquez sur « Créer une base de données ».
Remplissez le nom de la base de données et le type d’encodage,
ici `framadate` - `latin_swedish_ci`.
Une fois créée, cliquez sur « Privilèges » et « Créer un utilisateur ».
Remplissez les champs Serveur `localhost`, Utilisateur `framadate`,
Mot de passe, Privilèges `` `framadate`.* `` et cochez la case `All privileges`.

![](images/framadate/framadate-adminer.gif)

Par sécurité, vous pouvez supprimer le fichier `adminer.php` qui n’est plus nécessaire.

#### Framadate

Maintenant que la base de données est prête, il faut configurer
Framadate pour pouvoir s’en servir.

Rendez-vous ensuite sur la page `votre-nom-de-domaine.org/admin/install.php`.
Et remplissez le formulaire avec comme chaîne de connexion
`mysql:host=localhost;dbname=framadate;port=3306`, l'utilisateur `framadate`
et le mot de passe tel que vous l’avez défini à l'étape précédente.

![](images/framadate/framadate-install.png)

À l’installation, les tables de la base de données et le fichier
`app/inc/config.php` sont créés.
Le fichier `app/inc/config.php` contient d’autres paramètres de
configuration facultatifs que vous pouvez modifier.

Vous êtes ensuite redirigé vers la page de « migration » qui sert à
contrôler que les tables et données sont dans le bon format.
Pour les mises à jour ultérieures, il faudra passer par cette page après
remplacement des fichiers.

![](images/framadate/framadate-migration.png)

#### Espace admin

Framadate dispose d’un espace d’administration de l’ensemble des
sondages dans le dossier `/admin`

Pour en restreindre l’accès, il faut ajouter au fichier de configuration
Apache de votre site web (fichier `/etc/apache2/sites-enabled/votre-domaine.vhost`) ce bloc :

    <Directory "/var/www/framadate/admin/">
        AuthType Basic
        AuthName "Administration"
        AuthUserFile "/var/www/framadate/admin/.htpasswd"
        Require valid-user
        Order allow,deny
        Allow from all
    </Directory>

et créer le fichier `.htpasswd` contenant l’utilisateur et le mot de passe autorisé.

    htpasswd -bc /var/www/framadate/admin/.htpasswd utilisateur mot-de-passe

(l’utilitaire `htpasswd` se trouve dans le paquet `apache2-utils`)

Pour protéger les fichiers `.htaccess` et `.htpasswd`, pensez à ajouter également ceci :

    <FilesMatch "^\.ht.*">
    deny from all
    satisfy all
    ErrorDocument 403 "Accès refusé."
    </FilesMatch>


#### Réécriture d’URL

Pour activer la réécriture d’URL afin d’avoir des liens sous la forme
`http://votre-domaine.org/a1b2c3d4e5f6g7h8` au lieu de
`http://votre-domaine.org/studs.php?sondage=a1b2c3d4e5f6g7h8`.
Il faut ajouter une ligne `AllowOverride All` dans le fichier `.vhost`
pour autoriser l’utilisation des .htaccess sur votre domaine et renommer
le fichier `/var/www/framadate/htaccess.txt` en `/var/www/framadate/.htaccess`

Et voilà ! Normalement, ça pousse tout seul ;)

<i class="glyphicon glyphicon-tree-deciduous" aria-hidden="true"></i>

<p class="alert alert-info">
  <span class="label label-primary">Informations</span><br /> En complément,
  voici <a href="images/framadate/framadate-ubuntu.pdf">un tutoriel pour
  installer Framadate sur Ubuntu 14.04</a> de Patrick Harmel, sous
  licence <abbr>GFDL</abbr>.
</p>

 [1]: http://framadate.org
 [2]: https://framagit.org/framasoft/framadate/tags
 [3]: https://getcomposer.org
 [4]: https://framalibre.org/content/phpmyadmin
 [5]: http://www.adminer.org/