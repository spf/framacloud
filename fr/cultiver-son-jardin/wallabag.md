# Installation de Wallabag

[Wallabag][1] est une application de lecture différée : une page web
vous intéresse mais vous ne pouvez la lire à l’instant ?
Sauvegardez-là dans votre Wallabag pour la lire plus tard (sur navigateur, application mobile, ou liseuse).

Framasoft propose une instance de Wallabag nommée bien entendu… [Framabag][2] :-)
Pour une présentation plus complète et en images, n’hésitez pas à jeter
un œil à [notre présentation de Wallabag][3].

N’hésitez pas aussi à consulter la [documentation officielle de Wallabag][4],
riche en informations !

![](images/wallabag/Wallabag.png)

<div class="alert alert-info">
  <b>Deux installations sont possibles</b> : sur un serveur dédié, ou
  sur un hébergement mutualisé.
  Cependant, l’installation en mutualisé n’est pas celle que
  recommandent les développeurs et elle ne sera pas traitée dans ce
  tutoriel (votre serviteur n’a pas d’hébergement de test compatible).
  Ils fournissent néanmoins
  <a href="http://wllbg.org/latest-v2-package">un paquet contenant toutes les dépendances</a>
  facilitant le déploiement pour ceux qui le souhaitent.
</div>

## Prérequis

La machine utilisée pour ce tutoriel est la suivante :

*   Linux Debian Jessie
*   Serveur web Nginx installé
*   PHP 5.5 minimum (des configurations récentes avec PHP 7 fonctionnent aussi)

En option, vous pourriez avoir besoin :

*   d’un nom de domaine correctement configuré pour rediriger vers votre serveur
*   d’un serveur mail opérationnel pour confirmer les nouveaux comptes et la double authentification

<div class="alert alert-info">
  <b>Note</b> : pour installer PHP sur Debian avec Nginx, n'installez pas
  le paquet <code>php5</code> qui a pour dépendance Apache2, mais préférez
  le paquet <code>php5-fpm</code> (qui sera nécessaire pour la
  communication entre PHP et Nginx).
</div>

## Installation

### 1 - Préparer la terre

![](images/icons/preparer.png)

Ma chère Maïté, pour préparer la recette du Wallabag, il nous faut
quelques ustensiles !

Tout d’abord, nous allons avoir besoin de git pour récupérer les sources,
et de curl qui est utilisé en sous-main par Wallabag :

    sudo apt-get install git curl

Wallabag utilise aussi PHP pour fonctionner avec moult modules.
Rassurez-vous, la plupart sont fournis par défaut ! Pour ceux qui
manqueraient potentiellement :

    sudo apt-get install php5-curl php5-gd php5-sqlite php5-tidy

Pour vérifier que tout ce qu’il faut est installé, un petit `php -m`
vous indiquera quels sont les modules utilisés par PHP (en ligne de
commande – ce n’est pas forcément ce qui sera utilisé par le serveur web,
mais c'est un moyen simple et rapide de voir si le module est installé).

Vous pouvez aussi vérifier que les modules sont activés pour php-fpm,
en vérifiant quels sont les modules activés dans `/etc/php5/mods-available/`.
Si vous voyez une ligne du genre `extension=tidy.so` sans point-virgule devant, c’est activé.

Vous devez retrouver ces éléments dans la réponse de `php -m` :

    php-session
    php-ctype
    php-dom
    php-hash
    php-simplexml
    php-json
    php-gd
    php-mbstring
    php-xml
    php-tidy
    php-iconv
    php-curl
    php-gettext
    php-tokenizer

Afin de se connecter à une base de données, Wallabag utilise un des modules PDO.
Vous aurez donc besoin **d’au moins** une extension et d’un système de
bases de données au choix parmi :

    pdo_mysql
    pdo_sqlite
    pdo_pgsql

Pour la suite, nous utiliseront sqlite, qui est plus facile à configurer.

Enfin, pour gérer les dépendances PHP et construire l’application,
il nous faudra Composer :

    curl -s http://getcomposer.org/installer | php
    sudo mv composer.phar /bin/composer
    sudo chown root:root /bin/composer


(respectivement : on récupère l'installeur, on copie dans les exécutables
du système et on vérifie que les droits sont bien d'équerre)

### 2 - Semer

![](images/icons/semer.png)

Maintenant que tout est prêt, la météo est bonne, rentrons dans le vif du sujet !

On se place dans le répertoire de Nginx (dans notre installation,
`/var/www/html`) et on récupère les sources de Wallabag :

    cd /var/www/html
    sudo git clone https://github.com/wallabag/wallabag.git

Une fois que tout est téléchargé, on rentre dans le dossier fraîchement
créé, et on prépare l’installation avec Composer :

    cd wallabag
    SYMFONY_ENV=prod composer install --no-dev -o --prefer-dist

Au lancement de la dernière commande ci-dessus, Composer va préparer le
package Wallabag pour qu’il soit utilisable, et vous posera des questions
sur la configuration à générer.
Laissez les options par défaut si vous ne savez pas quoi mettre.

Quelques recommandations tout de même :

*   dans le cas de SQLite, bien mettre l’utilisateur `database_user`
    comme celui de nginx (ici `www-data`), sinon il lui sera difficile d’y accéder.
*   il est préférable (mais pas indispensable) de renseigner un serveur
    mail fonctionnel, car il peut servir pour créer et vérifier les comptes,
    ainsi que pour la double authentification (`twofactor`).

À titre d’exemple, la configuration utilisée dans le cadre de ce tutoriel :

    Creating the "app/config/parameters.yml" file
    Some parameters are missing. Please provide them.
    database_driver (pdo_sqlite):
    database_host (127.0.0.1):
    database_port (null):
    database_name (symfony):
    database_user (root): www-data
    database_password (null):
    database_path ('%kernel.root_dir%/../data/db/wallabag.sqlite'):
    database_table_prefix (wallabag_):
    mailer_transport (smtp):
    mailer_host (127.0.0.1):
    mailer_user (null):
    mailer_password (null):
    locale (en): fr
    secret (ovmpmAWXRCabNlMgzlzFXDYmCFfzGv):
    twofactor_auth (true): false
    twofactor_sender (no-reply@wallabag.org):
    fosuser_confirmation (true):
    from_email (no-reply@wallabag.org):

Vous voulez changer un paramètre plus tard ? Tant pis, il faudra tout
réinstaller ! ;-) Blague à part, ces paramètres peuvent se retrouver
dans le dossier d’installation de Wallabag, plus précisément dans le
fichier `app/config/parameters.yml`.

Une dernière commande pour finaliser l’installation du package préparé
précédemment :

    php bin/console wallabag:install --env=prod

La réponse attendue :

    Installing Wallabag...

    Step 1 of 5. Checking system requirements.
    +-----------------+--------+----------------+
    | Checked         | Status | Recommendation |
    +-----------------+--------+----------------+
    | PDO Driver      | OK!    |                |
    | curl_exec       | OK!    |                |
    | curl_multi_init | OK!    |                |
    +-----------------+--------+----------------+
    Success! Your system can run Wallabag properly.

    Step 2 of 5. Setting up database.
    Creating database and schema, clearing the cache

    Step 3 of 5. Administration setup.
    Would you like to create a new admin user (recommended) ? (Y/n)Y
    Username (default: wallabag) :
    Password (default: wallabag) : sup3rm0td3p4ssed3r0x0r
    Email:

    Step 4 of 5. Config setup.

    Step 5 of 5. Installing assets.

    Wallabag has been successfully installed.
    Just execute `php bin/console server:run --env=prod` for using wallabag: http://localhost:8000

(n’oubliez pas de renseigner – et mémoriser – le **compte admin** comme
demandé dans l’étape 3)

Enfin pour finaliser et faire bonne mesure, on s’assure que le dossier
`wallabag` fraîchement préparé appartient bien au même utilisateur que Nginx :

    sudo chown -R www-data:www-data /var/www/html/wallabag/

Voilà, Wallabag est installé ! Vous pouvez le tester avec un serveur
local en lançant `php bin/console server:run --env=prod` et en allant
voir sur `http://[ip-de-votre-serveur]:8000` (pour tester uniquement !).

### 3 - Arroser

![](images/icons/arroser.png)

La plupart d’entre vous voudront sûrement le mettre derrière un serveur
web (fortement recommandé si ce n’est pas un simple serveur de test !).
Dans notre configuration, il s’agit de Nginx. Les fichiers de configuration
des sites se trouvent dans `/etc/nginx/sites-available`, nous allons
donc créer la configuration à cet endroit :

    cd /etc/nginx/sites-available/
    nano wallabag.mydomain.tld

Reste à remplir ce fichier :

    server {
        server_name default_server;
        root /var/www/html/wallabag/web;

        location / {
            # try to serve file directly, fallback to app.php
            try_files $uri /app.php$is_args$args;
        }
        location ~ ^/app\.php(/|$) {
            fastcgi_pass unix:/var/run/php5-fpm.sock;
            fastcgi_split_path_info ^(.+\.php)(/.*)$;
            include fastcgi_params;

            fastcgi_param  SCRIPT_FILENAME  $realpath_root$fastcgi_script_name;
            fastcgi_param DOCUMENT_ROOT $realpath_root;
            # Prevents URIs that include the front controller. This will 404:
            # http://domain.tld/app.php/some-path
            # Remove the internal directive to allow URIs like this
            internal;
        }

        error_log /var/log/nginx/wallabag.error.log;
        access_log /var/log/nginx/wallabag.access.log;
    }

<div class="alert alert-info">
  <b>Notes</b> :
  <ul>
    <li>
        Attention ! pour ceux qui ont l’habitude de configurer avec un
        <code>index.html/php</code> à la racine, ici il faut pointer vers le
        sous-dossier <code>web</code> et le fichier <code>app.php</code>.
    </li>
    <li>
        Changez le <code>server_name</code> à votre sauce bien sûr,
        selon les domaines et redirections DNS que vous préférez (nous avons
        mis ici <code>default_server</code> car notre instance de test
        n’avait pas besoin de plus)
    </li>
  </ul>
</div>

Reste à activer cette configuration et redémarrer Nginx pour qu’il la prenne en compte :

    cd /etc/nginx/sites-enabled/
    sudo ln -s ../sites-available/wallabag
    sudo systemctl restart nginx

Plus qu’à aller sur l’adresse souhaitée (à défaut l’adresse IP du serveur)
et vous connecter avec le compte admin précédemment créé.

![](images/wallabag/Wallabag-login.png)

Dans l’état actuel, **si vous n’avez pas renseigné de serveur mail**,
les utilisateurs ne pourront pas créer de compte par eux-mêmes.
Deux solutions possibles : intercepter le mail qui ne s’enverra pas dans
votre mailqueue (`sudo mailq`), ou créer vous-même l’utilisateur via
l’interface de l’admin : `Configuration > Créer un compte`

![](images/wallabag/Wallabag-quickstart.png)

### 4 - Pailler

![](images/icons/pailler.png)

#### Bloquer l’inscription spontanée

Il n’est pas possible de **bloquer l’inscription de nouveaux utilisateurs**
dans l’écran de login pour l’instant. Le problème est [connu des développeurs][5]
et sera bientôt intégré. Une solution en attendant est de bloquer
l’enregistrement via Nginx en ajoutant dans le fichier :

    location ^~ /register {
      deny all;
      return 403;
    }

#### Mettre à jour wallabag

Il vous suffit de reprendre les dernières sources et de les recompiler avec composer.
Reprenez le chapitre « Semer » et procédez à l’installation, en reprenant
la base de données existante (faites un backup à minima de votre base
de données avant !
Comme on dit de l’autre côté de la Manche, *Better safe than sorry* :-) ).

 [1]: https://www.wallabag.org
 [2]: https://framabag.org/
 [3]: https://framabag.org/cquoi/#/
 [4]: http://doc.wallabag.org/fr/master/user/installation.html
 [5]: https://github.com/wallabag/wallabag/issues/1873