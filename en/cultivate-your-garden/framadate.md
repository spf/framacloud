# Framadate Installation

![](images/framadate/logo-framadate.png)

[Framadate][1] is the a creation software that we
develop and offer as an online service.
It is initially based on the Studs software developed by
the University of Strasbourg and that we have largely overhauled.

![](images/framadate/framadate.png)

Here is a tutorial to help you install it on your server.

<p class="alert alert-info">
  <span class="label label-primary">Information</span><br /> In the following tutorial we will give you the instructions for a Debian server with MySQL database, Apache web server (or NginX), PHP 5.4 (minimum), Postfix (or Exim, for sending emails) and Git installed.<br />
We assume that you have already pointed yout domain name on your server with your
<a href="https://en.wikipedia.org/wiki/Domain_name_registrar">registrar</a>.
</p>

## Installation

### 1 - Preparing the earth

![](images/icons/preparer.png)

First, log in as `root` on your server and create a user account `framadate` and the folder `/var/www/framadate` in which the files will be copied with the corresponding access rights.

    useradd framadate
    groupadd framadate
    mkdir /var/www/framadate
    chown framadate:framadate -R /var/www/framadate

### 2 - Sow

![](images/icons/semer.png)

Connect with the user `framadate` : `su framadate -s /bin/bash`

Then, download the files from our repository with Git in the folder `/var/www/framadate` and witch to [the latest stable version][2] (in the example 1.0.2).

    cd /var/www/framadate
    git clone https://framagit.org/framasoft/framadate.git .
    git checkout 1.0.2

Also install the php intl extension:

    sudo apt-get install php5-intl

Framadate relies on [Composer][3] software to import the libraries it depends on. To install it, run the command:

    php -r "readfile('https://getcomposer.org/installer');" | php

and import the necessary libraries

    ./composer.phar install

![](images/framadate/composer.png)

### 3 - Water

![](images/icons/arroser.png)

#### MySQL

We must now create the database and configure Framadate.

First install the `mysql-server` package (note the root password) and start MySQL: `service mysql start`

Create a user and database `framadate`.
On your dedicated server you can use [PhpMyAdmin][4] which is often pre-installed, otherwise here is how to do it with [Adminer][5].

Download Adminer with the command line (always from the folder `/var/www/framadate`).

    wget -O adminer.php http://www.adminer.org/latest-mysql.php

Log in with theMySQL root account on `domain-name.org/adminer.php`
and click on « Create Database ».
Fill in the name of the database and the type of encoding, in this case `framadate` - `latin_swedish_ci`.
Once created, click on « Privileges » and « Create User ».
Fill in the fields Server `localhost`, User `framadate`,
Password, Privileges `` `framadate`.* `` and check the `All privileges` box.

![](images/framadate/framadate-adminer.gif)

For security, you can delete the file `adminer.php` which is no longer needed.

#### Framadate

Now that the database is ready, you have to configure Framadate to use it.

Go to the page `domain-name.org/admin/install.php`.
And fill the form with the following string
`mysql:host=localhost;dbname=framadate;port=3306`, the user `framadate`
and the password as you defined it in the previous step.

<p class="alert alert-warning">
  If the page does not appear, check that a folder
  `/var/www/framadate/tpl_c` has been created with sufficient write permissions.
</p>

![](images/framadate/framadate-install.png)

Upon installation, the database tables and the file
`app/inc/config.php` are created.
The file `app/inc/config.php` contains other parameters of optional configurations that you can modify.

You are then redirected to the « migration » page which is used to check that the tables and data are in the correct format.
For future updates, you will have to go through this page after replacing files.

![](images/framadate/framadate-migration.png)

#### Admin area
Framadate has a management area for all polls in the folder `/admin`

To restrict access, add the following block to the Apache configuration file from you website
(`/etc/apache2/sites-enabled/votre-domaine.vhost`):

    <Directory "/var/www/framadate/admin/">
        AuthType Basic
        AuthName "Administration"
        AuthUserFile "/var/www/framadate/admin/.htpasswd"
        Require valid-user
        Order allow,deny
        Allow from all
    </Directory>

and createthe `.htpasswd` file containing the authorized user and password.

    htpasswd -bc /var/www/framadate/admin/.htpasswd utilisateur mot-de-passe

(the `htpasswd` ultility is in the `apache2-utils` package)

The protect the `.htaccess` et `.htpasswd` files, consider also adding this:

    <FilesMatch "^\.ht.*">
    deny from all
    satisfy all
    ErrorDocument 403 "Access denied."
    </FilesMatch>


#### URL rewriting

To enable URL rewriting, to have links in the form
`http://votre-domaine.org/a1b2c3d4e5f6g7h8` instead of
`http://votre-domaine.org/studs.php?sondage=a1b2c3d4e5f6g7h8`.
You can add the line `AllowOverride All` in the `.vhost` to allow the use of .htaccess on your domain and rename the file
`/var/www/framadate/htaccess.txt` to `/var/www/framadate/.htaccess`.

There you go! Normally, it grows by itself.

<i class="glyphicon glyphicon-tree-deciduous" aria-hidden="true"></i>

<p class="alert alert-info">
  <span class="label label-primary">Information</span><br /> In addition, here is <a href="images/framadate/framadate-ubuntu.pdf">
a tutorial for Framadate on Ubuntu 14.04 (in French)</a> by Patrick Harmel, under license <abbr>GFDL</abbr>.
</p>

Original tutorial in French by JosephK

 [1]: http://framadate.org
 [2]: https://framagit.org/framasoft/framadate/tags
 [3]: https://getcomposer.org
 [4]: https://framalibre.org/content/phpmyadmin
 [5]: http://www.adminer.org/

